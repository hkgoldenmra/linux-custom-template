#!/bin/bash

time2milliseconds(){
	ms=`echo "${1}" | awk -F '.' '{print $2}'`
	ss=`echo "${1}" | awk -F '.' '{print $1}'`
	hh=`echo "${ss}" | awk -F ':' '{print $1}'`
	mm=`echo "${ss}" | awk -F ':' '{print $2}'`
	ss=`echo "${ss}" | awk -F ':' '{print $3}'`
	echo $((10#$hh*3600000+10#$mm*60000+10#$ss*1000+10#$ms))
}

milliseconds2time(){
	ms=$((10#$1))
	hh=$(($ms/3600000))
	ms=$(($ms%3600000))
	mm=$(($ms/60000))
	ms=$(($ms%60000))
	ss=$(($ms/1000))
	ms=$(($ms%1000))
	printf "%02d:%02d:%02d.%03d" "${hh}" "${mm}" "${ss}" "${ms}"
}

duration(){
	e=`time2milliseconds "${2}"`
	s=`time2milliseconds "${1}"`
	echo $(($e-$s))
}

clip(){
	d=`duration "${1}" "${2}"`
	d=`milliseconds2time "${d}"`
	command="ffmpeg -y -i '${3}'"
	if [ "${#1}" -gt "0" ] && [ "${#2}" -gt "0" ]; then
		command="${command} -ss '${1}' -t '${d}'"
	elif [ "${#1}" -gt "0" ]; then
		command="${command} -ss '${1}'"
	elif [ "${#2}" -gt "0" ]; then
		command="${command} -t '${d}'"
	fi
	command="${command} -preset ultrafast -vcodec libx264 -vb 256k -acodec libmp3lame -ab 128k -ar 44100 -ac 1 '${3:0:-4}-${4}.mp4'"
	echo "${command}"
	eval "${command}"
}

#clip "00:00:00.000" "00:00:00.000" "input" "output"
